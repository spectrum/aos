AOS

An Operative System

Angelo Dureghello <angelo@sysam.it>, Sysam (C) 2015 Trieste, Italy


1. Introduction

This is a simple 64bit monokernel, mono-task operative system.
The purpose is to show how the basic layer of a simple operative 
system is done.

2. Setup

The OS can be executed or from a SD (see raw image), from emulators
like Bochs or similar that are able to process a raw image, or from 
VirtualBox using a vdi (virtualbox) image.

3. Steps to create a disk

Just run tools/setup, it prepares a disk forVirtualBox and/or as 
a raw image.

Enjoy.
