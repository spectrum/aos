/*
 * binutils: cd : main.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "stdio.h"
#include "string.h"
#include "unistd.h"
#include "stat.h"
#include "dirent.h"

int main(int argc, char **argv)
{
	char cur_path[MAX_PATH];

	/* linux move to home here */
	if (argc < 2) return 1;

	if (argv[1][0] == '.' && argv[1][1] == 0)
		return 1;

	if (getcwd(cur_path, MAX_PATH)) {

		if (strncmp(argv[1], "..", 2) == 0) {
			char * c = strrchr(cur_path, '/');

			printf("asked .. %s\r\n", cur_path);

			*(c+1) = 0;
			chdir(cur_path);

			return 0;

		} else {
			DIR * p = opendir(cur_path);

			if (p) {
				struct dirent *d;

				/* readdir_r, 0 on success */
				while ((d = readdir(p)) != 0) {
					if (strcmp(argv[1], d->d_name) == 0) {
						if(*cur_path != '/')
							strcat(cur_path, "/");
						strcat(cur_path, argv[1]);
						chdir(cur_path);

						return 0;
					}
				}
				printf("\r\n++err: no existing dir\r\n");
			}
		}
	}

	return 1;
}