/*
 * binutils: ls : main.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "stdio.h"
#include "string.h"
#include "unistd.h"
#include "stat.h"
#include "dirent.h"

int main()
{
	char cur_path[MAX_PATH];

	if (getcwd(cur_path, MAX_PATH)) {

		DIR * p = opendir(cur_path);

		if (p) {
			struct dirent *d;
			struct stat st;

			int offs, total = 0;

			offs = strlen(cur_path);

			/* readdir_r, 0 on success */
			while ((d = readdir(p)) != 0) {

				cur_path[offs] = '/';
				strcpy(&cur_path[offs + 1], d->d_name);

				if (!stat(cur_path, &st)) {
					printf("\x1b[33;1m%-16s", d->d_name);
					printf("\x1b[35m%d\r\n", st.st_size);

					total++;
				}
				else break;
			}
			printf("\x1b[36,1mtotal: %d files\x1b[0m\r\n", total);
		}
	}

	return 0;
}