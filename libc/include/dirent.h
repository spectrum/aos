#ifndef dirent_H_
#define dirent_H_

typedef unsigned char DIR;
typedef unsigned long ino_t;
typedef unsigned long off_t;

struct dirent {
	ino_t d_ino;			/* inode number */
	off_t d_off;			/* not an offset; see NOTES */
	unsigned short d_reclen;	/* length of this record */
	unsigned char d_type;		/* type of file */
	char d_name[256]; 		/* filename */
};

DIR *opendir(const char *name);
struct dirent *readdir(DIR *dirp);
int readdir_r(DIR *dir_ptr, struct dirent *entry, struct dirent **result);

#endif // dirent_H_