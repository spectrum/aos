#ifndef unistd_H_
#define unistd_H_

#define MAX_PATH   512

typedef unsigned int size_t;

int exec(const char *file);
char *getcwd(char *buf, size_t size);
int chdir(const char *path);

#endif // unistd_H_
