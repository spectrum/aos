/*
 * libc: dirent.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "dirent.h"
#include "string.h"

static DIR *curp = 0;

DIR * opendir(const char *name)
{
	DIR *dir_ptr = 0;

	__asm__ __volatile__ (
		"	mov	$0x21, %%al	\n"
		"	int	$0x80		\n"
		: "=a"(dir_ptr) : "S"(name)
	);

	return dir_ptr;
}


struct dirent d;

struct dirent *readdir(DIR *dir_ptr)
{
	if (!curp)
		curp = dir_ptr;

	/*
	 * we return an abstract dirent, that is different
	 * from aosfs dir entry actually, so we
	 * copy here only the POSIX mandatory fiels,
	 * as inode num and name.
	 */

	/*
	 * so, we need an intermediate syscall to have inode and
	 * name.
	 * Function also advance to next dir entry.
	 */
	__asm__ __volatile__ (
		"	mov	$0x22, %%al	\n"
		"	int	$0x80		\n"
		: "=S"(curp) : "c"(&d.d_ino), "D"(d.d_name), "S"(curp)
		:
	);

	/* inode 0 = root inode, so existing */
	if (d.d_ino == 0xffff) {
		curp = 0;
		return 0;
	}

	return &d;
}

int readdir_r(DIR *dir_ptr, struct dirent *entry, struct dirent **result)
{
	/* internal, to close */
	if (dir_ptr == 0) {

	}

	if (!curp) {
		curp = dir_ptr;
	}

	entry->d_ino = *(unsigned short *)&curp[0]; // inode

	if (entry->d_ino == 0xffff) {
		return 1;
	}

	strcpy(entry->d_name, (char *)&curp[2]);

	*result = (struct dirent *)curp;

	curp += 32;

	return 0;
}