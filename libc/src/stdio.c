/*
 * libc: stdio.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "stdio.h"

int putchar(int c)
{
	return c;
}

int puts(const char *s)
{
	int printed;

	__asm__ __volatile__ (
		"	mov	$0x03, %%al	\n"
		"	int	$0x80		\n"
		: "=a"(printed)
		: "S"(s)
		:
	);

	return printed;
}

int printf(const char *format, ...)
{
	int printed;
	va_list l;
	va_start(l, format);

	__asm__ __volatile__ (
		"	mov	$0x01, %%al	\n"
		"	int	$0x80		\n"
		: "=a"(printed)
		: "S"(format), "D"(l)
		:
	);

	return printed;
}

int getch()
{
	int c = 0;

	__asm__ __volatile__ (
		"	mov	$0x02, %%al	\n"
		"	int	$0x80		\n"
		: "=a"(c)
		:
		:
	);

	return c;
}

