/*
 * libc: unistd.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "unistd.h"

int exec(const char *file)
{
	int res;

	__asm__ __volatile__
	(
		"	mov	$0x30, %%al	\n"
		"	int	$0x80		\n"
		: "=a"(res)
		: "D"(file)
	);

	return res;
}

char *getcwd(char *buf, size_t size)
{
	char *rval = 0;

	__asm__ __volatile__
	(
		"	mov	$0x20, %%al	\n"
		"	int	$0x80		\n"
		: "=a"(rval)
		: "D"(buf), "c"(size)
	);

	return rval;
}

int chdir(const char *path)
{
	int res;

	__asm__ __volatile__
	(
		"	mov	$0x24, %%al	\n"
		"	int	$0x80		\n"
		: "=a"(res)
		: "S"(path)
	);

	return res;
}
