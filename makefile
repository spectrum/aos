
# AOS global makefile

SHELL := /bin/bash

# order very important
SUBDIRS = libc mbr os binutils

all:
	@for i in $(SUBDIRS); do \
	( $(MAKE) -C $$i $@ ) || exit $$?; done

clean:
	@for i in $(SUBDIRS); do \
	(cd $$i; make clean); done

