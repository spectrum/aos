#ifndef __hd_h
#define __hd_h

char _hd_read_chs (uint16_t c, uint16_t h, uint16_t s,
			size_t sect_count, unsigned char *sbuff);
char _hd_read_lba (uint32_t lba_address, size_t sect_count, unsigned char *sbuff);


#endif // __hd_h
