/*
 * os: modules: hd.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "../../os/include/kmsgs.h"
#include "../include/ktypes.h"

#define SIZE_SECT 512

/*
 * standard ide hd access driver
 *
 * angelo dureghello (C) nuvon inc, 2007
 *
 * 1) Before doing anything with a device you have to wait till it
 * indicates that it is ready (RDY bit in the status register)
 *
 * 2) Next you load the parameters of a command into the appropriate
 * registers. For read/write commands that comes down to writing
 * the cylinder/head/sector numbers into the registers.
 *
 * 3) You issue a read or write command.
 *
 * 4) You wait till the device signals that it is ready for data
 * transfer (DRQ in the status register).
 *
 * 5) Feed the device data (for write) or get the data from the
 * device (for read). In case of a write you could wait for the
 * operation to complete and read the status register to find out
 * what has become of your data.
 *
 * +----+------+------+---+---+---+----------------+---------------+
 * |Addr|-CS1FX|-CS3FX|SA2|SA1|SA0| Read (-IOR)    | Write (-IOW)  |
 * +----+------+------+---+---+---+----------------+---------------+-----------+
 * |1F0 |  0   |  1   | 0 | 0 | 0 | Data Port      | Data Port     | <--+      |
 * |1F1 |  0   |  1   | 0 | 0 | 1 | Error Register | Precomp       |    |      |
 * |1F2 |  0   |  1   | 0 | 1 | 0 | Sector Count   | Sector Count  | Command   |
 * |1F3 |  0   |  1   | 0 | 1 | 1 | Sector Number  | Sector Number | Block     |
 * |1F4 |  0   |  1   | 1 | 0 | 0 | Cylinder Low   | Cylinder Low  | Registers |
 * |1F5 |  0   |  1   | 1 | 0 | 1 | Cylinder High  | Cylinder High |    |      |
 * |1F6 |  0   |  1   | 1 | 1 | 0 | Drive / Head   | Drive / Head  |    |      |
 * |1F7 |  0   |  1   | 1 | 1 | 1 | Status         | Command       | <--+      |
 * +----+------+------+---+---+---+----------------+---------------+-----------+
 *
 */

__asm__ (
	/* wait disk ready */
	".macro _d_rdy			\n"
	"	mov	$0x1f7, %dx	\n"
	"1:	inb	%dx, %al	\n"
	"	bt	$6, %rax	\n"
	"	jnc	1b		\n"
	".endm				\n"
	".macro _b_rdy			\n"
	"	mov	$0x1f7, %dx	\n"
	"1:	inb	%dx, %al	\n"
	"	bt	$3, %rax	\n"
	"	jnc	1b		\n"
	".endm				\n"
);


char _hd_read_chs (uint16_t c, uint16_t h, uint16_t s,
			size_t sectors, unsigned char *sbuff)
{
	/* TODO if ever will be needed */

	return 1;
}


char _hd_read_lba(
	uint32_t lba_address, size_t sectors, unsigned char *sbuff)
{
	/*
	 * steps
	 *
	 * 1 - before any operation on the hd, we wait disk rdy
	 * 2 - setup address and issue read
	 * 3 - wait for DRQ bit to be set
	 * 4 - read (bochs-multi)sector data
	 *
	 * NOTE: bochs here allows to read whole multisector data
	 *       while virtualbox, more picky, want you wait DRQ for
	 *       each sector (loop to 3).
	 *
	 * Functions stay modified for all emulators to work.
	 *
	 *
	 */
	__asm__ __volatile__ (
		"	and	$0xfffffff, %%rax	\n"
		"	mov	%%rax, %%rbx		\n"
		"	_d_rdy				\n"
		"	mov	$0x1f6, %%dx		\n"
		"	shr	$24, %%eax		\n"
		"	or	$0xe0, %%al		\n"
		"	out	%%al, %%dx		\n"
		"	mov	$0x1f2, %%dx		\n"
		"	mov	%%cl, %%al		\n"
		"	out	%%al, %%dx		\n"
		"	inc	%%dx			\n"
		"	mov	%%rbx, %%rax		\n"
		"	out	%%al, %%dx		\n"
		"	inc	%%dx			\n"
		"	mov	%%rbx, %%rax		\n"
		"	shr	$8, %%rax		\n"
		"	out	%%al, %%dx		\n"
		"	inc	%%dx			\n"
		"	mov	%%rbx, %%rax		\n"
		"	shr	$16, %%rax		\n"
		"	out	%%al, %%dx		\n"
		"	mov	$0x1f7, %%dx		\n"
		"	mov	$0x20, %%al		\n"
		"	out	%%al, %%dx		\n"
		"	mov	%%rcx, %%r8		\n"
		"check:					\n"
		"	_b_rdy				\n"
		"	mov	$256, %%rcx		\n"
		"	mov	$0x1f0, %%dx		\n"
		"	rep	insw			\n"
		"	dec	%%r8			\n"
		"	cmp	$0, %%r8		\n"
		"	jne	check			\n"
		:
		: "c"(sectors), "a"(lba_address), "D"(sbuff)
		: "%rdx"
	);

	return 1;
}
