#ifndef _kio_h
#define _kio_h

__asm__
(	".macro inp port		\r\n"
	"	mov	\\port, %dx	\r\n"
	"	inb	%dx, %al	\r\n"
	".endm				\r\n"
	".macro outp port, value	\r\n"
	"	mov	\\port, %dx	\r\n"
	"	mov	\\value, %al	\r\n"
	"	outb	%al, %dx	\r\n"
	".endm				\r\n"
);

#endif // _kio_h
