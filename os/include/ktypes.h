#ifndef ktypes_H
#define ktypes_H

#define true	1
#define false	0

#include <stddef.h>
#include <stdint.h>

typedef unsigned char	BOOL;

#endif
