#ifndef _string_h
#define _string_h

#include "ktypes.h"

#ifdef __GNUC__
typedef __builtin_va_list va_list;
#endif

/*
 * zero pad
 */
char * zpad(char *sz, int times);

/*
 * integer to ascii
 */
char * itoa(int val);

/*
 * ascii to integer
 */
int atoi(char *s);

int kstrcmp(char *s1, char *s2);
char * kstrchr(char *src, int val);
void kstrcat(char *dst, char *src);
void kstrcpy(char *dst, char *src);
void kstrncpy(char *dst, char *src, int len);
int kstrlen(char *s);
void kprint(const char *string);
void kprintc(const char *string, int c);
int kprintf_s(char* s, va_list vl);
int kprintf(char* s, ...);
void kmemset(unsigned char *dst, int val, size_t count);
void kfmemset(unsigned char *dst, int val, size_t count);
void kmemcpy(unsigned char *dst, unsigned char *src, size_t count);
void kfmemcpy(unsigned char *dst, unsigned char *src, size_t count);
void kxmemcpy(unsigned char *dst, unsigned char *src, size_t count);

#endif // _string_h