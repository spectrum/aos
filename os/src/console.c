/*
 * console.c: console layer over video driver
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "screen.h"
#include "console.h"
#include "string.h"

#include "../../modules/include/vga.h"

static int color = DEFAULT_COLOR;

static int translate_to_video_color(int color)
{
	int vc;

	switch(color) {
	case 30: vc = CL_BLACK;		break;
	case 31: vc = CL_RED;		break;
	case 32: vc = CL_GREEN;		break;
	case 33: vc = CL_BROWN; 	break;
	case 34: vc = CL_BLUE;		break;
	case 35: vc = CL_MAGENTA;	break;
	case 36: vc = CL_CYAN;		break;
	case 37: vc = CL_LT_GRAY;	break;
	default:
		vc = DEFAULT_COLOR;
	break;
	}

	return vc;
}

static void set_terminal_color(int fore, int back, int bright)
{
	int vc = translate_to_video_color(fore);

#ifdef GRAPHIC_MODE
	/* TODO : back */

	if (bright) vc += 8;
	vga_select_color(vc);
#else
	textmode_set_color(vc);
#endif
}

static const int sz_reset_seq = 4;

int vt100_parse(const char* vt100_seq)
{
#ifdef VT100_SUPPORT
	char* s = (char *)vt100_seq;
	char *last, *q;
	int val, bright = 0, back, fore;

	/* is valid esc seq ? */
	if (*(unsigned short*)s != 0x5b1b) return 0;

	s += 2;

	for(;;) {
		last = 0; q = 0;
		if ((q = kstrchr(s, ';')) || (last = kstrchr(s, 'm'))) {
			val = atoi(s);
			if (val == 0 && last) {
				/* reset */
				console_unset_char_color();
				return sz_reset_seq;
			}
			if (val < 10) {
				/*  attr */
				if (val == 1) bright = 1;
			} else if (val >=30 && val <= 39) {
				/* fore */
				fore = val;
			} else {
				back = val;
			}

			if (last) {
				set_terminal_color(fore, back, bright);
				return ((last + 1) - vt100_seq);
			} else {
				s = q + 1;
			}
		}
		else {
			/* invalid seq */
			return 0;
		}
	}
#else
	return 0;
#endif
}

static void console_apply_color()
{
#ifdef GRAPHIC_MODE
	vga_select_color(color);
#else
	textmode_set_color(color);
#endif
}

void console_set_char_color(int c)
{
	color = c;

	console_apply_color();
}

void console_unset_char_color()
{
	color = DEFAULT_COLOR;

	console_apply_color();
}

void console_print(const char *s)
{
#ifdef GRAPHIC_MODE
	vga_print_string(s);
#else
	textmode_print_string((char *)s);
#endif
}

struct _driver_vga drv;

void console_init()
{
	console_set_char_color(CL_GREEN);

#ifdef GRAPHIC_MODE
	drv.console_parse = vt100_parse;

	vga_driver_init(&drv);
#endif
}
