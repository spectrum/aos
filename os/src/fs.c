/*
 * os : afs access functions
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 *	AFS is a basic file system.
 * 	The structure adopted is the following
 *
 *	+-------------------------+
 *	|       superblock        |  1024 Bytes
 *	+-------------------------+
 *	| free block bitmap table |  min 1 block (note block, not sect)
 *	+-------------------------+
 *	| free inode bitmap table |  min 1 block (note block, not sect)
 *	+-------------------------+
 *	|       inode table       |  x blocks
 *	+-------------------------+
 *	|  files                  |  all in blocks
 *	\                         |
 *            ................... |
 *
 */

#include "fs.h"
#include "kmsgs.h"
#include "string.h"

// modules
#include "hd.h"

/* absolute byte offset in the inode table */
#define __inode_n_to_byte_pos(n) (n * sizeof(struct _s_inode))

// global accessible directory buffer, maximum od 4096 entries
unsigned char _dir[_SIZE_DIR + 1];
unsigned char _dir_tmp[_SIZE_DIR + 1];

unsigned char *dirp = _dir_tmp;

// hold the superblock
static struct _superblock _sb;
// general buffer for i/o operations
static unsigned char _block[_SIZE_BLOCK + 1];
// partition table
static unsigned char _ptable[_SIZE_PT_ENTRY * 4];

#define _purge_slash(x) { while(**x == '/') (*x)++; }

/**
 * key file system absolute coordinates
 */
struct s_boot_data {
	uint32_t lba_start;
	uint32_t lba_tot_sectors;
	uint32_t lba_sb;
};

/*
 * initial path is the root
 */
char curr_path[_MAX_PATH] = {"/"};

static struct s_boot_data boot;

static int _read_pt()
{
	_hd_read_lba(0, 1, _block);

	if (_block[510] == 0x55 && _block[511] == 0xAA)
	{
		// getting fs info
		kmemcpy(_ptable, &_block[_OFFS_PTABLE], sizeof(_ptable));

		/* _lba values are in sectors */
		boot.lba_start = *(uint32_t *)&_ptable[8];
		boot.lba_tot_sectors = *(uint32_t *)&_ptable[12];
		boot.lba_sb = boot.lba_start + _OFFS_SUPERBLOCK;

		/* all other values will be read from superblock */
		return 1;
	}
	return 0;
}

/**
 * low level, to do as driver module
 */
int _fs_read_superblock()
{
	if (_read_pt()) {

		_hd_read_lba(boot.lba_sb, _SECT_CNT_SUPERBLOCK, _block);

		if (*(uint32_t *)_block == 0x20736f61 &&
			*(uint32_t *)&_block[4] == 0x65707573 ) {

			kmemcpy((unsigned char*)&_sb, _block, sizeof(_sb));

			/* check eventually validity of superblock */

			return 1;
		}
		else
			kerr("bad superblock\r\n");
	}
	return 0;
}

/*
 * read a directory in memory
 */
static int _read_directory(struct _s_inode * i)
{
	int offset=0, x=0;

	for (;x<10 && i->_ptrs[x]; x++, offset += _SIZE_BLOCK) {

		if (!_hd_read_lba(i->_ptrs[x], _SECT_CNT_BLOCK, &dirp[offset]))
			return 0;
	}
	dirp[offset + _SIZE_BLOCK] = 0;

	return 1;
}

static unsigned char *__entry = (unsigned char *)_BINARY_ENTRY;

/**
 * read a directory in memory
 * TODO: check for errors and block the loading
 * TODO: add crc to binaries
 */
static void _read_binary(struct _s_inode * i)
{
	size_t offset = 0, x = 0;

	for (;x < 10 && i->_ptrs[x]; x++, offset += _SIZE_BLOCK) {
		_hd_read_lba(i->_ptrs[x], _SECT_CNT_BLOCK, &__entry[offset]);
	}
}

/*
 * get inode from current directory
 */
inode_n _fs_get_inode(char *file_name, uint32_t dir_size)
{
	struct _dentry	*e;
	int offset = 0;

	for (;dir_size; dir_size -= sizeof(struct _dentry)) {
		e = (struct _dentry*)&dirp[offset];

		if (kstrcmp(e->_ename, file_name) == 0) return e->_inode;

		offset += sizeof(struct _dentry);
	}

	return _INODE_NOT_FOUND;
}

/*
 * _fs_read_inode
 *
 * input: inode n, _s_inode *
 *
 * Considering that inode struct size may vary.
 * NOTE: no need to read a block (block aligned), we can read
 * the exact sector, so that for sector sizes smaller than
 * block sizes it is faster.
 * TODO: add a check for valid inode, for example file size != 0
 */
int _fs_read_inode(inode_n n, struct _s_inode *i)
{
	int rv;
	uint32_t sector = (__inode_n_to_byte_pos(n) / _SIZE_SECT);
	uint32_t offset = (__inode_n_to_byte_pos(n) % _SIZE_SECT);

	if (_SIZE_SECT - offset < sizeof(struct _s_inode)) {
		/*
		 * TODO: special case, inode is between 2 sectors:
		 * in this case we will need to read 2 sectors
		 * containing the inode struct.
		 *
		 * Since is supposed a setup tool (i.e. Linux, but
		 * a setup tool could be based from AOS itself) can
		 * seek and write at exact byte, we accept this
		 * _s_inode divided in 2 sectors.
		 */
		kerr("TODO: inode between two sectors\r\n");

		return 0;
	}

	/* we read only 1 interested sector (using block buffer btw) */
	rv = _hd_read_lba(_sb.lba_i_table + sector, 1, _block);

	if (rv) {
		kmemcpy((unsigned char *)i, _block + offset,
			sizeof(struct _s_inode));

		if (i->_uid == 0 || i->_fsize == 0) {
			kerr("\r\ninvalid inode, critical error\r\n");

			rv = 0;
		}
	}
	else {
		kerr("inode lba read error!\r\n");
	}
	return rv;
}

static int _fs_get_root_directory(struct _s_inode *i)
{
	if (_fs_read_inode(0, i)) {
		if (_read_directory(i)) {
			return 1;
		}
	}

	return 0;
}

static int _fs_pop_directory(char **file_path, struct _s_inode *i)
{
	int rval = 0;

	if (*file_path[0] == '/') {
		/* first time, inode struct must be read */
		if (_fs_get_root_directory(i)) {
			_purge_slash(file_path);
			rval = 1;
		}
	} else {
		/*
		 * now an inode struct is set,
		 * we have : file_or_dir/file_or_dir/etc
		 */
		char *c = kstrchr(*file_path, '/');
		inode_n n;

		/* i contains last directory info */
		if (!c) {
			return 0;
		}
		*c++ = 0;

		if ((n = _fs_get_inode(*file_path, i->_fsize))
			!= _INODE_NOT_FOUND) {

			if (_fs_read_inode(n, i)) {
				if(_read_directory(i)) {
					*file_path = c;
					rval = 1;
				}
			}
		}
	}

	return rval;
}

BOOL _fs_load_binary_in_memory(char *file_path, unsigned char *dest_addr)
{
	struct _s_inode i;
	inode_n n;
	BOOL rval = 0;

	dirp = _dir_tmp;

	if (dest_addr)
		__entry = dest_addr;

	while (_fs_pop_directory(&file_path, &i));

	/*
	 * here we are on the last directory of the tree,
	 * to load the file in memory
	 */
	if ((n = _fs_get_inode(file_path, i._fsize)) != _INODE_NOT_FOUND) {
		if (_fs_read_inode(n, &i)) {
			_read_binary(&i);
			rval = 1;
		}
		else {
			go_into_panic("error reading inode\r\n");
		}
	}

	dirp = _dir;

	return rval;
}

long syscall_getcwd(char *buff, size_t size)
{
	long res = 0;

	if (size) {
		kstrcpy(buff, curr_path);
		res = (long)buff;
	}

	return res;
}

long syscall_opendir(char *path_name)
{
	static char file_path[_MAX_PATH];
	struct _s_inode i;
	char *p = file_path;

	dirp = _dir_tmp;

	/*
	 * pointers can be const *, so we have no rights to
	 * modify the pointed content
	 */
	kstrcpy(p, path_name);
	kstrcat(p, "/");

 	while (_fs_pop_directory(&p, &i));

	dirp = _dir;

	if (_read_directory(&i)) {
		/*
		 * NOTE: we return here a ptr to _dir aosfs dir entry
		 */
		dirp = _dir_tmp;

		return (long)_dir;
	}

	dirp = _dir_tmp;

	return 0;
}

long syscall_read_dirent_entry(long dir_ptr, char *name, unsigned long *inode)
{
	struct _dentry *e = (struct _dentry *)dir_ptr;

	/* afs with generic direntry binding */
	kstrcpy(name, e->_ename);
	*inode = (unsigned long)e->_inode;

	/* advance */
	dir_ptr += sizeof(struct _dentry);

	return dir_ptr;
}

long syscall_get_file_info(char *file_name)
{
	static char file_path[_MAX_PATH];
	struct _s_inode itmp;
	inode_n n;
	long rval = -1;
	char *p = file_path;

	kstrcpy(p, file_name);

	dirp = _dir_tmp;

	while (_fs_pop_directory((char **)&p, &itmp));

	/*
	 * here we are on the last directory of the tree,
	 * to load the file in memory
	 */
	if ((n = _fs_get_inode(p, itmp._fsize)) != _INODE_NOT_FOUND) {
		if (_fs_read_inode(n, &itmp)) {
			rval = itmp._fsize;
		}
	}

	dirp = _dir;

	return rval;
}

long syscall_chdir(char* name)
{
	kstrcpy(curr_path, name);

	return 0;
}