/*
 * os: interrupts.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
 
#include "interrupts.h"

#include "ktypes.h"
#include "kmsgs.h"
#include "kio.h"
#include "string.h"
#include "fs.h"
#include "math.h"
#include "syscalls.h"
#include "tasks.h"

#define _INTERRUPTS	256

extern void _irq_keyboard(struct _irq_regs *r);

extern void kmemset(unsigned char *dst, int val, size_t count);
extern void kcursor_state (BOOL);
extern void kmemcpy(unsigned char *dst, unsigned char *src, size_t count);

const char *exception[]	= {
	"Divide Fault",
	"Exception 01",
	"NMI Interrupt",
	"Exception 03",
	"Overflow Trap",
	"Bounds Check Fault",
	"Invalid Opcode Fault",
	"Coprocessor Not Available Fault",
	"Double Fault",
	"Coprocessor Segment Overrun",
	"Invalid Task State Segment Fault",
	"Not Present Fault",
	"Stack Fault",
	"General Protection Fault",
	"Page Fault",
	0,
	"Floating-Point Exception-Pending",
	"Alignment-Check Exception",
	"Machine-Check Exception",
	"Floating-Point Exception"
};

/* 64bit ia32_64 */
struct _idt_reg {
	uint16_t size;
	uint64_t base;
} __attribute__((packed));

/* 64bit ia32_64 */
struct _idt_gate_desc {
	uint16_t offset_procedure_0_15;
	uint16_t segment_selector;
	uint8_t zeropad1;
	uint8_t mask;
	uint16_t offset_procedure_16_31;
	uint32_t offset_procedure_32_63;
	uint32_t zeropad2;
} __attribute__((packed));

static struct _idt_gate_desc *_idt_start = 0x00000000;

struct _idt_reg  idtr;

/*
 *	@ HW RESET :
 *	IRQ0 to IRQ7  are mapped to IDT entries 8 through 15.
 *	IRQ8 to IRQ15 are mapped to IDT entries 0x70 through 0x78.
 */
	 
void *_irq_routines[19] =
{
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0
};

void *_isr_routines[_INTERRUPTS];


/*
 * syscalls
 */

extern void syscall(struct _irq_regs *r);

void _int_0x80(struct _irq_regs *r)
{
	syscall(r);
}

/* IRQ are 0 to 15 */
void _setup_custom_irq_handler(int irq, void (*handler)(struct _irq_regs *r))
{
	_irq_routines[irq] = handler;
}

/* IRQ are 0 to 15 */
void _setup_custom_isr_handler(int irq, void (*handler)(struct _irq_regs *r))
{
	_isr_routines[irq - 0x80] = handler;
}

static void _setup_idt_isr(uint32_t int_n, uint64_t linear_offset)
{
	struct _idt_gate_desc  igd;
	
	kmemset	((unsigned char *)&igd, 0, sizeof(struct _idt_gate_desc));
	
	igd.offset_procedure_0_15 = (uint16_t) linear_offset & 0xffff;
	igd.segment_selector = 0x08;
	igd.mask = 0x8e;
	igd.zeropad1 = 0x00;
	igd.offset_procedure_16_31 =
		(uint16_t) ((linear_offset >> 16) & 0xffff);
	igd.offset_procedure_32_63 =
		(uint32_t) ((linear_offset >> 32) & 0xffffffff);
	
	_idt_start[int_n] = igd;
}

/** 
 * to leave isr handlers 0 to 15 free for cpu exceptions, 
 * IRQ0 to 15 will be remapped to IDT entries 32 to 47 
 **/
void _pic_irq_remap ()
{
	__asm__ __volatile__ (	
		"	outp	$0x20, $0x11	\n"
		"	outp	$0xA0, $0x11	\n"
		"	outp	$0x21, $0x20	\n"
		"	outp	$0xA1, $0x28	\n"
		"	outp	$0x21, $0x04	\n"
		"	outp	$0xA1, $0x02	\n"
		"	outp	$0x21, $0x01	\n"
		"	outp	$0xA1, $0x01	\n"
		"	outp	$0x21, $0x00	\n"
		"	outp	$0xA1, $0x00	\n"
	);
}

void k_load_ivt ()
{
	/* remap irq 0 - 15 riordinandoli fora dalle balle */
	_pic_irq_remap();

	_setup_idt_isr(0x00, (uint64_t)&_isr_00);
	_setup_idt_isr(0x01, (uint64_t)&_isr_01);
	_setup_idt_isr(0x02, (uint64_t)&_isr_02);
	_setup_idt_isr(0x03, (uint64_t)&_isr_03);
	_setup_idt_isr(0x04, (uint64_t)&_isr_04);
	_setup_idt_isr(0x05, (uint64_t)&_isr_05);
	_setup_idt_isr(0x06, (uint64_t)&_isr_06);
	_setup_idt_isr(0x07, (uint64_t)&_isr_07);
	_setup_idt_isr(0x08, (uint64_t)&_isr_08);
	_setup_idt_isr(0x09, (uint64_t)&_isr_09);
	_setup_idt_isr(0x0A, (uint64_t)&_isr_0A);
	_setup_idt_isr(0x0B, (uint64_t)&_isr_0B);
	_setup_idt_isr(0x0C, (uint64_t)&_isr_0C);
	_setup_idt_isr(0x0D, (uint64_t)&_isr_0D);
	_setup_idt_isr(0x0E, (uint64_t)&_isr_0E);
	_setup_idt_isr(0x10, (uint64_t)&_isr_10);
	_setup_idt_isr(0x11, (uint64_t)&_isr_11);
	_setup_idt_isr(0x12, (uint64_t)&_isr_12);
	_setup_idt_isr(0x13, (uint64_t)&_isr_13);
	
	/* isr of IRQ 0 to 15, remapped on the PIC, as isr 0x20 to 0x2F */
	_setup_idt_isr(0x20, (uint64_t)&_isr_20);
	_setup_idt_isr(0x21, (uint64_t)&_isr_21);
	_setup_idt_isr(0x22, (uint64_t)&_isr_22);
	_setup_idt_isr(0x23, (uint64_t)&_isr_23);
	_setup_idt_isr(0x24, (uint64_t)&_isr_24);
	_setup_idt_isr(0x25, (uint64_t)&_isr_25);
	_setup_idt_isr(0x26, (uint64_t)&_isr_26);
	_setup_idt_isr(0x27, (uint64_t)&_isr_27);
	_setup_idt_isr(0x28, (uint64_t)&_isr_28);
	_setup_idt_isr(0x29, (uint64_t)&_isr_29);
	_setup_idt_isr(0x2A, (uint64_t)&_isr_2A);
	_setup_idt_isr(0x2B, (uint64_t)&_isr_2B);
	_setup_idt_isr(0x2C, (uint64_t)&_isr_2C);
	_setup_idt_isr(0x2D, (uint64_t)&_isr_2D);
	_setup_idt_isr(0x2E, (uint64_t)&_isr_2E);
	_setup_idt_isr(0x2F, (uint64_t)&_isr_2F);
	
	_setup_idt_isr(0x80, (uint64_t)&_isr_80);

	_setup_custom_irq_handler(0x01, &_irq_keyboard);
	_setup_custom_irq_handler(0x08, &_irq_rtc_clock);
	_setup_custom_isr_handler(0x80, &_int_0x80);
	
	idtr.size = _INTERRUPTS * sizeof(struct _idt_gate_desc) - 1;
	idtr.base = 0x0;

	__asm__ __volatile__ (
		"lidt	(%%rsi)		\n"
		"sti			\n"
		: : "S"(&idtr)
	);
}

typedef void (*handler)(struct _irq_regs *r);


void _isr_page_fault(int error)
{
	kerr("page fault\r\n");

	switch(error) {
	case 0x00:
		/*
		 * 0  0  0
		 * Supervisory process tried to read a non-present page entry
		 */
		kerr("tried to read a non-present page entry\r\n");
	break;
	case 0x02:
		/*
		 * 0  1  0
		 * Supervisory process tried to write to a non-present page entry
		 */
		kerr("tried to write to a non-present page entry\r\n");
	break;

	default:
		show_error_code(error);
	break;
	}

	for(;;);
}

void _isr_handler(struct _irq_regs *r)
{
	if (r->int_no < 32) {
		/* special handling */
		switch(r->int_no) {
		case 14:
			_isr_page_fault(r->err_code);
		break;
		default:
			if ((r->int_no >= 10 && r->int_no <= 14) ||
				(r->int_no == 17))  {
				show_error_code(r->err_code);
			}
			go_into_panic(exception[r->int_no]);
		break;
		}
	} else {
		handler callback = _isr_routines[r->int_no - 0x80];
		
		if (callback) {
			callback(r);
		}
	}
}

void _irq_handler(struct _irq_regs *r)
{
	handler callback = _irq_routines[r->int_no - 32];
	
	if (callback) {
		callback(r);
	}

	/*
	 * If the IDT entry that was invoked was greater than 40(0x28),
	 * (meaning IRQ8 - 15),
	 * then we need to send an EOI to the slave controller
	 */
	if (r->int_no >= 0x28) {
		__asm__ __volatile__
		(
			"outp	$0xA0, $0x20	\n"
		);
	}

	/*
	 * In either case, we need to send an EOI to the master
	 * interrupt controller too
	 */
	__asm__ __volatile__
	(
		"outp	$0x20, $0x20	\n"
	);
}
