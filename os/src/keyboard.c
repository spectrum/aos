/*
 * os: keyboard.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
#include "interrupts.h"
#include "string.h"
#include "kio.h"

/*
 * US Keyboard Layout. This is a scancode table
 * used to layout a standard US keyboard.
 */
unsigned char uskl[128] =
{
	0,  27,
	'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '\b',
	'\t','q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',
	0 /* Control */,
	 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '`',
	0 /* l shift */, '\\', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/',
	0 /* r shift */,
	'*',
	0,	/* Alt */
	' ',	/* Space bar */
	0,	/* Caps lock */
	0,	/* 59 - F1 key ... > */
	0,   0,   0,   0,   0,   0,   0,   0,
	0,	/* < ... F10 */
	0,	/* 69 - Num lock*/
	0,	/* Scroll Lock */
	0,	/* Home key */
	0,	/* Up Arrow */
	0,	/* Page Up */
	'-',
	0,	/* Left Arrow */
	0,
	0,	/* Right Arrow */
	'+',
	0,	/* 79 - End key*/
	0,	/* Down Arrow */
	0,	/* Page Down */
	0,	/* Insert Key */
	0,	/* Delete Key */
	0,   0,   0,
	0,	/* F11 Key */
	0,	/* F12 Key */
	0,	/* All other keys are undefined */
};


struct _keyboard_buffer {
#define _KB_SIZE		32
	unsigned head;
	unsigned tail;
	unsigned char buffer[_KB_SIZE];
};

static struct _keyboard_buffer k;

void _irq_keyboard(struct _irq_regs *r)
{
	unsigned char scancode;

	//kprint ("keyboard isr\r\n");

	/* Read from the keyboard's data buffer */
	__asm__ __volatile__ (
		"inp	$0x60		\n"
		: "=a"(scancode) :
	);

	if (scancode & 0x80) {

	} else {
		unsigned char head, tail;
		//kprint ("keyboard key pressed\r\n");

		head  = k.head & (_KB_SIZE-1);
		tail  = k.tail & (_KB_SIZE-1);

		if ( ((head+1) & (_KB_SIZE-1)) == tail )
		{
			kprint ("keyboard buffer full!!\r\n");
		}
		else
		{
			k.buffer[head] = uskl[scancode];

			k.head++;
		}
	}
}

int syscall_getchar()
{
	char value[2] = {0};

	// waiting for input
	while (k.head == k.tail) {
			__asm__ __volatile__ (
			"sti		\n"
			"nop		\n"
			"nop		\n"
			"nop		\n"
			"cli		\n"
		);
	}

	value[0] = (int)k.buffer[k.tail & (_KB_SIZE-1)];
	k.tail++;

	// redirection, i do it here inside the kernel
	kprint(value);

	if (value[0] == 10)
	{
		kprint("\r");
	}

	return (int)value[0];
}