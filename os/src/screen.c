/*
 * os: text mode video driver
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "screen.h"
#include "kio.h"

unsigned long cur_x = 0;
unsigned long cur_y = 0;

void kcursor_state(BOOL state)
{
	__asm__ __volatile__(
		"	outp	$0x3d4, $0x0a		\n"
		"	inp	$0x3d5			\n"
		"	or	%%ebx, %%ebx		\n"
		"	jnz	1f			\n"
		"	or	$0x020, %%al		\n"
		"	jmp	2f			\n"
		"1:	and	$0x0df, %%al		\n"
		"2:	outp	$0x3d5, %%al		\n"
		:
		: "b"((unsigned long)state)
	);
}

void kupdate_cursor()
{
	*(unsigned char*)(0xb8000 + (((cur_y * 80 + cur_x) * 2))) = ' ';
	*(unsigned char*)(0xb8000 + (((cur_y * 80 + cur_x) * 2) + 1)) = 0x0e;

	unsigned long temp = cur_y * 80 + cur_x;

	__asm__ __volatile__(
		"	outp	$0x3d4, $0x0e		\n"
		"	outp	$0x3d5, %%bh		\n"
		"	outp	$0x3d4, $0x0f		\n"
		"	outp	$0x3d5, %%bl		\n"
		:
		: "b"(temp)
	);
}

void kclear_screen()
{
	// screen is 80x25

	cur_y = cur_x = 0;

	__asm__ __volatile__ (
		"	xor	%ecx, %ecx		\n"
		"1:	movw	$0x0041, 0xb8000(%ecx)	\n"
		"	add	$2, %ecx		\n"
		"	cmp	$0xfa0, %ecx		\n"
		"	jl	1b			\n"
	);
}

void kscroll_line()
{
	// screen is 80x25

	__asm__ __volatile__ (
		"	pushw	%si			\n"
		"	push	%rax			\n"
		"	push	%rdx			\n"
		"	mov	$160, %esi		\n"
		"	mov	$960, %dx		\n"
		"1:	movl	%gs:(%esi), %eax	\n"
		"	movl	%eax, %gs:-160(%esi)	\n"
		"	add	$4, %esi		\n"
		"	dec	%dx			\n"
		"	or	%dx, %dx		\n"
		"	jnz	1b			\n"
		"	mov	$3840, %esi		\n"
		"	mov	$0x20, %al		\n"
		"	mov	$80, %dl		\n"
		"2:	movb	%al, %gs:(%esi)		\n"
		"	dec	%dl			\n"
		"	add	$2, %esi		\n"
		"	or	%dl, %dl		\n"
		"	jnz	2b			\n"
		"	pop	%rdx			\n"
		"	pop	%rax			\n"
		"	popw	%si			\n"
	);
}

#define fb_start	((char *)0xb8000)
#define fb_cur_offs	((cur_y * 160) + (cur_x << 1))

static int tcolor;

void textmode_set_color(int color)
{
	tcolor = color;
}

void textmode_print_string(char *string)
{
	char *fb = fb_start;
	char *sp = (char *)string;

	fb += fb_cur_offs;

	while (*sp) {
		if (*sp == 0x0a) {
			cur_y++;
			fb = fb_start + fb_cur_offs;
			sp++;
			continue;
		}
		if (*sp == 0x0d) {
			cur_x = 0;
			fb = fb_start + fb_cur_offs;
			sp++;
			continue;
		}
		*fb++ = *sp++;
		*fb++ = tcolor;
		cur_x++;
	}

	kupdate_cursor();
}