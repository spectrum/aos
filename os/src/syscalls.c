/*
 * os: syscalls.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "string.h"
#include "syscalls.h"
#include "interrupts.h"

/*
 * System call are bridges to the OS, called mainly from the libc
 *
 * eax contains the sycall id
 * other regs used as params depending from the all
 */

extern long syscall_printf(char* s, va_list vl);
extern long syscall_getchar();
extern long syscall_puts(const char *s);
extern long syscall_getcwd(char *buff, size_t size);
extern long syscall_opendir(char *path_name);
extern long syscall_read_dirent_entry(long dir_ptr, char* name,
					unsigned long *indoe);
extern long syscall_get_file_info(char *file_path);
extern long syscall_chdir(char *path);
extern long syscall_exec(char* binary);

enum syscalls {
	printf = 0x01,
	getchar,
	puts,
	getcwd = 0x20,
	opendir,
	read_dirent_entry,
	get_file_info,
	chdir,
	exec = 0x30,
};

void syscall(struct _irq_regs *r)
{
	int service = r->rax & 0xff;

	switch (service) {
	case printf:
		r->rax = syscall_printf((char *)(r->rsi), (void *)r->rdi);
	break;
	case getchar:
		r->rax = syscall_getchar();
	break;
	case puts:
		r->rax = syscall_puts((char*)r->rsi);
	break;
	case getcwd:
		r->rax = syscall_getcwd((char *)r->rdi, (size_t)r->rcx);
	break;
	case opendir:
		r->rax = syscall_opendir((char *)r->rsi);
	break;
	case read_dirent_entry:
		r->rsi = syscall_read_dirent_entry(r->rsi, (char*)r->rdi,
						   (unsigned long*)r->rcx);
	break;
	case get_file_info:
		r->rax = syscall_get_file_info((char*)r->rsi);
	break;
	case chdir:
		r->rax = syscall_chdir((char*)r->rsi);
	break;
	case exec:
		r->rax = syscall_exec((char*)r->rdi);
	break;
	default:
		kprintf("int 80, bad service, %d\r\n", service);
	break;
	}
}
