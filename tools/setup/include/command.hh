/**
 * command.hh
 *
 * command prompt tools
 *
 **/

#include <string>

using std::string;
 
class cmd 
{
public:
	int igetch ();

	void console_command(const std::string &command);
	void console_mode_start ();
	void console_mode_stop ();
};