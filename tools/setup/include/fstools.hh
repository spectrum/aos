/**
 * fstools.hh
 *
 * file system tools
 *
 **/

#include <string>
#include <vector>
#include <fstream>
#include <sys/types.h>

#include "../../../os/include/fs.h"

using std::string;
using std::vector;
using std::fstream;

namespace fst {

typedef std::vector<unsigned long> ptrs;

enum image
{
	MEDIA_IMG_256M,
};

class fsaccess
{
	fsaccess();

public:
	static fsaccess &get()
	{
		static fsaccess f;

		return f;
	}

private:
	void create_image(unsigned int size);
	int  get_program_path(string &path);
	size_t  get_file_size(fstream &f);

	bool file_exist(const string &f);

	int  get_free_slot(vector<unsigned char> &);
	int  get_free_block ();
	int  get_free_inode();
	int  get_inode(const string &in, bool parent = false);
	void load_block_bitmap(off_t lba_offset, size_t byte_size);
	void load_inode_bitmap(off_t lba_offset, size_t size);
	void load_inode_struct(int inode, struct _s_inode &);
	void load_directory(int inode);
	void step_forward(const string &folder);

	int write_directory(const string &name, vector<string> &e);

	void write_inode(off_t offset, ptrs &vct, int size = 0);
	void write_dir_entry(unsigned short inode,
			const char *name, unsigned long offset);
	void write_file(const string & name, const string & file);
	void write_mbr(image size);

public:
	int setup_hd_image(image size);
	void setup_hd_image_vbox(image size);
	void erase_all_images();

	void write_vbr();
	void write_kernel();
	void write_fs();
	int write_binaries();

private:
	int f;
	struct _superblock sb;
	string ppath, target;
	vector<unsigned char> bbitmap, ibitmap;
	unsigned char _block[_SIZE_BLOCK + 1];
};

}
