/**
 * uiapp.hh
 * visual support for setup tool
 *
 **/
#ifndef __visual_hh
#define __visual_hh

#include "command.hh"

#include <string>
#include <vector>

using std::string;
using std::vector;

struct uirect
{
	uirect() {}
	uirect(int rx, int ry, int rw, int rh) : x(rx), y(ry), w(rw), h(rh) {}
	int x, y, w, h;
};

class uiobject
{
public:
	virtual ~uiobject() {}
	virtual void redraw() = 0;

protected:
	void set_cursor(int x, int y);

protected:
	uirect r;
};


class uiframe;

typedef void (uiframe::*cb)();

struct uimenuentry
{
	uimenuentry(int k, cb cbk, string d) : key(k), callback(cbk), desc(d)
	{}

	int key;
	cb callback;
	string desc;
};

typedef vector<uimenuentry> entry_list;


struct uimenu : public uiobject
{
	uimenu(int x, int y, entry_list &e);

	virtual void redraw();

public:
	entry_list entries;
};

struct uiframe : public uiobject
{
	uiframe();
	uiframe(const string &title, int x, int y, int width, int height);

	virtual ~uiframe() {}
	virtual void redraw();

protected:
	void status_clear();
	void status_error(const string &err);

public:
	virtual int exec() = 0;

protected:
	string title;
	vector <uiobject *> clients;
};

struct uiframe_main : public uiframe
{
	uiframe_main(int width, int height);
	
	virtual int exec();
	virtual void redraw();

private:
	void on_create_img_256M();
	void on_create_img_256M_vbox();
	void on_erase_imgs();
	void on_exit();

private:
	uimenu *mm;
	cmd cl;
};

struct uiapp 
{
	uiapp ();
	
	int exec ();
		
private:
	uiframe_main  *fm;
};

#endif // __visual_hh