/**
 * fstools.cc
 *
 * rw fs tools
 *
 **/

#include "fstools.hh"
#include "command.hh"

#include <string>
#include <fstream>
#include <iostream>

#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <stdlib.h>

using namespace std;

namespace fst {

static const int MAX_PATH = 512;
static const int MAX_FILE_NAME = 256;

fsaccess::fsaccess ()
{
	get_program_path(ppath);
}

int fsaccess::get_program_path (string &path)
{
	char dname[MAX_PATH];
	int rval = 0;
	pid_t pid;
	struct dirent *entry;
	DIR *d;
	char *p;

	pid = getpid();

	snprintf(dname, sizeof(dname), "/proc/%d", pid);

	if ((d=opendir(dname))!=NULL) {
		while ((entry = readdir(d)) != NULL) {
			if ((strncmp(entry->d_name, "exe", 3))==0) {
				strcat(dname, "/exe");
				readlink(dname, dname, MAX_PATH + MAX_FILE_NAME);
				if ((p = strrchr(dname, '/')) != NULL) *p = 0;

				path = dname;
				rval = 1;

				break;
			}
		}
		closedir(d);
	}
	return rval;
}

size_t fsaccess::get_file_size(fstream &f)
{
	size_t size;

	f.seekg (0, std::ios::end);
	size = f.tellg();
	f.seekg (0, std::ios::beg);

	return size;
}

bool fsaccess::file_exist(const string &f)
{
	fstream fo(f.c_str(), fstream::in);

	return fo.is_open();
}

void fsaccess::write_inode(off_t lba_bytes_offset, ptrs &vct, int size)
{
	struct _s_inode is;

	lseek(f, lba_bytes_offset, SEEK_SET);
	memset(&is, 0, sizeof(struct _s_inode));

	is._uid = 0x01;
	is._gid = 0x01;
	is._rights = 0xff;
	is._fsize = size;

	for (size_t i = 0; i < vct.size(); i++) {
		is._ptrs[i] = vct[i];
	}
	write(f, &is, sizeof(struct _s_inode));
}

/**
 * directory is a list of entries describing the directory files
 *
 * inode  entry
 * inode  entry
 * ....
 *
 **/

void fsaccess::write_dir_entry ( unsigned short inode,
			const char *name,
			unsigned long offset )
{
	static uint16_t eoe = 0xffff;
	struct	_dentry	e;

	memset(&e, 0, sizeof (struct _dentry));

	// cout << "name, " << e._ename << " writing inode " << inode << "\r\n";

	e._inode = inode;
	strcpy(e._ename, name);
	lseek(f, offset, SEEK_SET);
	write(f, &e, sizeof (struct _dentry));
	/* mark e of entries */
	write(f, &eoe, 2);
}

/*
 * We keep fixed 16heads and 63 spt
 * We need to calculate cylinders based on size
 *
 * note: cyl/head count start from 0
 *
 */
void fsaccess::write_mbr(image size)
{
	int m;

	if ((f = open(target.c_str(), O_RDWR)) != -1)
	{
		// write MBR now
		string mbr;

		/*
		 * fixed part, active, start VBR sector at
		 * 128 (0x10000), 0x30 for AOS
		 */

		mbr = ppath + "/" + "../../../mbr/mbr";

		if ((m = open(mbr.c_str(), O_RDWR)) != -1) {
			char buffer[_SIZE_SECT + 1];

			read(m, buffer, _SIZE_SECT);

			lseek(f, 0, SEEK_SET);
			write(f, buffer, _SIZE_SECT);

			close(m);
		}
		else {
			close(f);
			return;
		}

		unsigned long disk_size;
		unsigned long heads = 16;
		unsigned long spt = 63;
		/* we start at 0x10000 */
		unsigned long start_lba = 128;
		/* fixed start for LBA 128 (0x10000) */
		unsigned char chs_start[5] = {0x80, 0x00, 0x01, 0x02, 0x30};

		switch (size) {
		default:
		case MEDIA_IMG_256M: {
			disk_size = 1024 * 1024 * 256;
		}
		break;
		}

		/* last lba sector, 0 based */
		unsigned long end_lba = disk_size / _SIZE_SECT - 1;

		/*
		 * reverse LBA to CHS
		 */
		unsigned long end_cyl = end_lba / (heads * spt);
		unsigned long tmp = end_lba % (heads * spt);
		unsigned char end_head = (unsigned char)(tmp / spt);
		unsigned char end_sect = (unsigned char)((tmp % spt) + 1);

		unsigned char end_c_s = ((end_cyl & 0x300)  >> 2) | end_sect;
		unsigned char end_c_l = (end_cyl & 0xff);

		// 1 to substract is MBR
		unsigned long tot_sects = (end_lba + 1) - start_lba;

		lseek(f, 446, SEEK_SET); // move to partition table
		write(f, &chs_start, 5);
		write(f, &end_head, 1);
		write(f, &end_c_s, 1);
		write(f, &end_c_l, 1);
		write(f, &start_lba, 4);
		write(f, &tot_sects, 4);

		close(f);
	}
}

void fsaccess::write_vbr()
{
	if ((f = open(target.c_str(), O_RDWR)) != -1) {
		string kernel = ppath + "/" + "../../../os/vbr/vbr";
		fstream fi(kernel.c_str(), ios::in);
		int size = get_file_size(fi);

		char * fmem = new char[size + 1];

		if (fmem) {
			fi.read(fmem, size);

			lseek(f, _SECT_CNT_MBR * _SIZE_SECT, SEEK_SET);
			write(f, fmem, size);

			fi.close();
		}
		close (f);
	}
}

void fsaccess::write_kernel()
{
	if ((f = open(target.c_str(), O_RDWR)) != -1) {
		string kernel = ppath + "/" + "../../../os/osbin";
		fstream fi(kernel.c_str(), ios::in);
		int size = get_file_size(fi);

		char * fmem = new char[size + 1];

		if (fmem) {
			fi.read(fmem, size);

			lseek(f, (_SECT_CNT_MBR + _SECT_CNT_VBR) * _SIZE_SECT,
			      SEEK_SET);
			write(f, fmem, size);

			fi.close();
		}
		close (f);
	}
}

void fsaccess::write_fs()
{
	cmd cmdtools;
	string command = ppath;

	command += "/../../../tools/mkafs/mkafs " + target;

	cmdtools.console_command(command);
}

void fsaccess::load_block_bitmap(off_t lba_offset, size_t byte_size)
{
	bbitmap.resize(byte_size);

	lseek(f, lba_offset * _SIZE_SECT, SEEK_SET);
	read(f, &bbitmap[0], byte_size);
}

/*
 * scanning the bitmap to get a free inode
 */
int fsaccess::get_free_slot (vector<unsigned char> &v)
{
	unsigned int  i, x;
	unsigned char z;
	unsigned long slot = 0;

	for (i = 0; i < v.size(); ++i) {
		z = v[i];
		//cout << "get_free_slot() : z = " << ((int)(z) & 0xff) << "\n";
		for (x = 0; x < 8; ++x, z <<= 1) {
			if (!(z & 0x80)) { v[i] |= (1 << (7 - x)); return slot; }
			else
				slot++;
		}
	}
	return -1;
}

int fsaccess::get_free_block()
{
	int b = get_free_slot(bbitmap);

	lseek(f, sb.lba_b_bitmap * _SIZE_SECT, SEEK_SET);
	write(f, &bbitmap[0], bbitmap.size());

	return b;
}

int fsaccess::get_free_inode()
{
	int i = get_free_slot(ibitmap);

	lseek(f, sb.lba_i_bitmap * _SIZE_SECT, SEEK_SET);
	write(f, &ibitmap[0], ibitmap.size());

	return i;
}

void fsaccess::load_inode_bitmap(off_t lba_offset, size_t size)
{
	ibitmap.resize(size);

	lseek(f, lba_offset * _SIZE_SECT, SEEK_SET);
	read(f, &ibitmap[0], size);
}

void fsaccess::load_inode_struct(int inode, struct _s_inode &s)
{
	lseek(f, sb.lba_i_table * _SIZE_SECT +
		(inode * sizeof(struct _s_inode)), SEEK_SET);
	read(f, (char*)&s, sizeof(struct _s_inode));
}

void fsaccess::load_directory(int inode)
{
	struct _s_inode inode_struct;

	load_inode_struct(inode, inode_struct);

	lseek(f, inode_struct._ptrs[0] * _SIZE_SECT, SEEK_SET);
	read(f, &_block, _SIZE_BLOCK);
}

void fsaccess::step_forward(const string &part)
{
	struct _dentry *d = (struct _dentry *)&_block;

	for (int x = 0; d[x]._ename[0] != 0; ++x) {
		if (part == d[x]._ename)
		{
			load_directory(d[x]._inode);

			break;
		}
	}
}

int fsaccess::get_inode (const string &in, bool parent)
{
	uint16_t inode;
	size_t i;

	string fpath = in;

	if (fpath[0] == '/') {
		inode = 0;
		load_directory(inode);
		fpath.erase(0, 1);
	}

	for (;;) {
		if ((i = fpath.find('/')) != string::npos) {
			string part=fpath.substr(0,i);
			step_forward(part);
			fpath.erase(0,i+1);
		} else {
			//last
			struct _dentry *d = (struct _dentry *)&_block;
			int iparent = -1;

			for (int x = 0; d[x]._ename[0] != 0; ++x) {
				if (d[x]._ename[0] == '.') iparent = d[x]._inode;
				if (fpath == d[x]._ename)
					return parent ? iparent : d[x]._inode;
			}
			break;
		}
	}
	return -1;
}

static void erase_and_rewrite_line(const string & str)
{
	usleep(100000);
	printf("\x1b[A\x1b[2K");
	rewind(stdout);
	printf(str.c_str());
	printf("\n");
}

int fsaccess::write_directory(const string &name, vector<string> &e)
{

	uint32_t fb = get_free_block();
	uint32_t lba_block = sb.lba_block_0 +
		(fb * _SECT_CNT_BLOCK);

	//cout << "write_directory : get_free_block() = " << fb << "\n";
	//cout << "write_directory : sb.lba_block_0 = " << sb.lba_block_0 << "\n";
	//cout << "write_directory : free block " << lba_block  << "\n";

	erase_and_rewrite_line(name);

	int inode, pinode, i, offs = lba_block * _SIZE_SECT;

	if (name == "/") {
		inode = get_free_inode();
		if (inode == -1) {
			cout << "bad inode table !!\r\n";
			return 0;
		}
		pinode = 0;
	} else {
		inode = get_inode(name);
		pinode = get_inode(name, true);
	}

	write_dir_entry(inode, ".", offs ); offs += sizeof(struct _dentry);
	write_dir_entry(pinode, "..", offs ); offs += sizeof(struct _dentry);

	for (i = 0; i < (int)e.size(); ++i) {
		write_dir_entry(get_free_inode(), e[i].c_str(), offs);
		offs += sizeof(struct _dentry);
	}
	// no 0 termination needed if formatted

	ptrs p;
	p.clear();
	p.push_back(lba_block);
	write_inode((sb.lba_i_table * _SIZE_SECT) +
		(inode * sizeof(struct _s_inode)), p, 4096);

	return 1;
}

void fsaccess::write_file(const string &name, const string &file)
{
	string abs_path(ppath);

	abs_path.append("/");
	abs_path.append(file);

	erase_and_rewrite_line(name);

	std::fstream fi(abs_path.c_str(), std::ios::binary | std::ios::in);

	if (fi.is_open()) {
		size_t size = get_file_size(fi);
		char * fmem =
			new char[size + (_SIZE_BLOCK - (size % _SIZE_BLOCK)) + 1];

		fi.read(fmem, size);

		if (fmem) {
			int q;
			int s = size;
			int finode = get_inode(name);
			ptrs p;

			p.clear();

			do {
				q = sb.lba_block_0 +
					(get_free_block() * _SECT_CNT_BLOCK);

				lseek(f, q * _SIZE_SECT, SEEK_SET);
				write(f, fmem, _SIZE_BLOCK);
				fmem += _SIZE_BLOCK;

				p.push_back(q);
			}
			while (s /= _SIZE_BLOCK);

			write_inode((sb.lba_i_table * _SIZE_SECT) +
				(finode * sizeof(struct _s_inode)),
				p,
				size
				);
		}
		fi.close();
	}
}

int fsaccess::write_binaries()
{
	off_t file_offset =
		(off_t)(_SECT_CNT_MBR + _SECT_CNT_VBR
			+ _SECT_CNT_KERNEL) * _SIZE_SECT;

	if ((f = open(target.c_str(), O_RDWR)) != -1) {
		vector<string> entries;
		ptrs p;
		string fname;

		// first step, reading superblock to get info
		// cout << "* reading superblock\r\n";

		lseek(f, file_offset, SEEK_SET);
		read(f, (unsigned char*)&sb, sizeof(struct _superblock));

		// cout << "* signature        : " << sb.sign << "\n";
		// cout << "* lba block bitmap : " << sb.lba_b_bitmap << "\n";
		// cout << "* lba inode bitmap : " << sb.lba_i_bitmap << "\n";
		// cout << "* lba inode table  : " << sb.lba_i_table << "\n";
		// cout << "* lba block 0      : " << sb.lba_block_0 << "\n";

		/** load blocks bitmap, if some block is used **/
		load_block_bitmap(sb.lba_b_bitmap,
			(sb.lba_i_bitmap - sb.lba_b_bitmap) * _SIZE_SECT);
		load_inode_bitmap(sb.lba_i_bitmap,
			(sb.lba_i_table - sb.lba_i_bitmap) * _SIZE_SECT);

		// now start to write the files
		entries.clear();
		entries.push_back("system");
		entries.push_back("home");
		entries.push_back("global");

		if (! write_directory("/", entries))
			return 0;

		entries.clear();
		if (! write_directory("/home", entries))
			return 0;

		entries.clear();
		if (! write_directory("/global", entries))
			return 0;

		entries.clear();
		entries.push_back("ashell");
		entries.push_back("ls");
		entries.push_back("cd");

		if (! write_directory("/system", entries))
			return 0;

		fname = "../../../binutils/ashell/ashell";
		write_file("/system/ashell", fname.c_str());

		fname = "../../../binutils/ls/ls";
		write_file("/system/ls", fname.c_str());

		fname = "../../../binutils/cd/cd";
		write_file("/system/cd", fname.c_str());

		cout << "\n\nfiles installed succesfully\r\n";

		close(f);
	}
	else
		cout << "*   can't open device\r\n";

	fflush(stdout);

	return 1;
}

void fsaccess::create_image (unsigned int size)
{
	fstream f(target.c_str(), ios::out | ios::binary);

	while(size--)
		f.put(0);

	f.flush();
	f.close();
}

int fsaccess::setup_hd_image(image size)
{
	int rval = 1;
	cmd cmdtools;

	cmdtools.console_mode_start();

	cout << "writing image ...\n";

	target = ppath + "/";

	/*
	 * as per MBR default pt scheme, we keep 16heads and 63spt,
	 * varying cylinders
	 */

	switch(size) {
	case MEDIA_IMG_256M:
		/*
		 * Part created as from cfdisk,
		 * Geometry: 7 heads, 12 sectors/track, 1016 cylinders
		 * 80 01 03 04 83 06 cc f8
		 */
		target += "media_256M.img";
		if (file_exist(target)) {
			cout << "image file already exist, exiting ...\n";
			return 1;
		}
		create_image(1024 * 1024 * 256);
	break;
	}

	/*
	 * linux bootable image examples
	 *
	 * ** NOTE, WE COULD DO WHAT WE WANT HERE BUT RESPECTING BOOT
	 *    SPEC CAN BE SAFE TO HAVE A BOOTABLE OS FROM SD **
	 *
	 * 00 82 03 00 0c a5 1e 07 00 20  00 00 00 c0 01 00  (boot at 0x400000)
	 * 00 82 05 08 83 10 f0 ff 00 00  02 00 00 08 ee 00
	 *
	 */
	if (file_exist(target)) {
		cout << "writing mbr ...       ";
		write_mbr(size);
		cout << "[  OK  ]\n";


		cout << "writing vbr and kernel ...    ";
		write_vbr();
		write_kernel();
		cout << "[  OK  ]\n";

		cout << "mkfs.aos ...          ";
		write_fs();
		cout << "[  OK  ]\n";

		cout << "installing files ...\n\n";
		if (!write_binaries()) {
			cout << "error, aborting\n";
			rval = 0;
			goto exit;
		}
	} else {
		cout << "image file missing or damaged ...\n";
		goto exit;
	}
	cout << "image created successfully\n";

exit:
	system("sync");
	cmdtools.console_mode_stop();

	return rval;
}

void fsaccess::setup_hd_image_vbox(image size)
{
	cmd cmdtools;

	if (!setup_hd_image(size))
		return;

	string cmd =
		"VBoxManage convertdd ";

	switch(size) {
	case MEDIA_IMG_256M:
		cmd += ppath + "/media_256M.img ";
		cmd += ppath + "/media_256M.vdi ";
	break;
	}
	cmd += "--format VDI";


	cmdtools.console_mode_start();
	system(cmd.c_str());
	system("sync");
	cmdtools.console_mode_stop();
}

void fsaccess::erase_all_images()
{
	cmd cmdtools;
	string erase_cmd;

	cmdtools.console_mode_start();
	erase_cmd = string("rm -f ") + ppath + "/*.img";
	cout << erase_cmd << "\n";
	system(erase_cmd.c_str());
	system("sync");
	erase_cmd = string("rm -f ") + ppath + "/*.vdi";
	cout << erase_cmd << "\n";
	system(erase_cmd.c_str());
	system("sync");
	cmdtools.console_mode_stop();
}

}
