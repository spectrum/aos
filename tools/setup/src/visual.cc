/**
 * uiapp.cc
 * visual support for setup tool
 *
 **/

#include "visual.hh"
#include "fstools.hh"

#include <iostream>
#include <sstream>
#include <cstdio>
#include <cstdlib>

#include <unistd.h>
#include <sys/ioctl.h>

using namespace std;
using namespace fst;

/* all in vt100 */

static const char vt100_erase_line[] = "\x1b[2K";
static const char vt100_clear_scr[] = "\x1b[1;1H\x1b[2J";
static const char vt100_reset[] = "\x1b[0m";
static const char vt100_hide_cursor[] = "\x1b[?25l";
static const char vt100_show_cursor[] = "\x1b[?25h";

static const char vt100_bold[] = "1";

static const char vt100_b_red[] = "41";

static const char vt100_f_black[] = "30";
static const char vt100_f_yellow[] = "33";
static const char vt100_f_cyan[] = "36";


static const string vt100_seq = "\x1b[";

/* compositions */
static const string color_frame_border = vt100_seq + vt100_f_yellow + "m";
static const string color_frame_title = vt100_seq + vt100_f_cyan + "m";
static const string color_status_error =
	vt100_seq +
	vt100_bold + ";" +
	vt100_b_red + ";" +
	vt100_f_yellow + "m";

void uiobject::set_cursor(int x, int y)
{
	cout << "\x1b[" << y << ";" << x << "f";
}

uiframe::uiframe() {}

uiframe::uiframe(const string &frame_title, int x, int y, int width, int height) :
title(frame_title)
{
	r.x = x;
	r.y = y;
	r.w = width;
	r.h = height;

	redraw();
}

void uiframe::redraw()
{
	cout << color_frame_border;

	int title_x_pos = (r.w - title.length()) / 2;

	set_cursor(r.x, r.y);
	cout << "\u2554";
	for (int i = r.x + 1; i < (r.x + r.w - 2); ++i) {
		if (i == title_x_pos) {
			cout << "\u2561 ";
			cout << color_frame_title << title << color_frame_border;
			cout << " \u255e";
			i += title.length() + 3;
			continue;
		}
		cout << "\u2550";
	}
	cout << "\u2557";
	for (int i = r.x + 2; i < (r.y + r.h - 2); ++i) {
		set_cursor(0, i);
		cout << "\u2551\n";
		set_cursor(r.w - 1, i);
		cout << "\u2551\n";
	}
	cout << "\u255a";
	for (int i = r.x + 1; i < (r.x + r.w - 2); ++i)
		cout << "\u2550";
	cout << "\u255d";

	cout << "\n";
}

void uiframe::status_clear()
{
	set_cursor(r.x, r.h - 1);
	cout << vt100_erase_line;
}

void uiframe::status_error(const string &err)
{
	set_cursor(r.x, r.h - 1);
	cout << color_status_error;
	cout << vt100_erase_line << ">>> " << err << vt100_reset << "\n";
	cout << char(7);

	usleep(200000);
}

uimenu::uimenu(int x, int y, entry_list &e) : entries(e)
{
	r.x = x;
	r.y = y;

	redraw();
}

void uimenu::redraw()
{
	for (unsigned int i = 0; i < entries.size(); ++i) {
		set_cursor(r.x, r.y + i);
		cout << "\u2609" << " " << entries[i].desc << "\n";
	}
}

uiframe_main::uiframe_main(int width, int height)
: uiframe("AOS Setup Tool", 0, 0, width, height)
{
	entry_list e;

	e.push_back(uimenuentry(0x31, (cb)&uiframe_main::on_create_img_256M,
				"1. Create 256MB (default) SD/HD raw image"));
	e.push_back(uimenuentry(0x32, (cb)&uiframe_main::on_create_img_256M_vbox,
				"2. Create 256MB (default) SD/HD vdi image"));
	e.push_back(uimenuentry(0x39, (cb)&uiframe_main::on_erase_imgs,
				"9. Erase images"));
	e.push_back(uimenuentry(0x71, (cb)&uiframe_main::on_exit,
				"q. Exit Setup Tool"));

	mm = new uimenu(5, 3, e);

	clients.push_back(mm);
}

void uiframe_main::redraw()
{
	uiframe::redraw();

	for (size_t i = 0; i < clients.size(); ++i)
		((uiobject *)clients[i])->redraw();
}

void uiframe_main::on_create_img_256M()
{
	fsaccess::get().setup_hd_image(MEDIA_IMG_256M);
}


void uiframe_main::on_create_img_256M_vbox()
{
	fsaccess::get().setup_hd_image_vbox(MEDIA_IMG_256M);
}

void uiframe_main::on_erase_imgs()
{
	fsaccess::get().erase_all_images();
}

void uiframe_main::on_exit()
{
	cout << vt100_clear_scr;
	cout << vt100_show_cursor;
	exit(0);
}

int uiframe_main::exec()
{
	for (;;) {

		status_clear();

		int val = cl.igetch();
		size_t i;

		for (i = 0; i < mm->entries.size(); ++i) {
			if (mm->entries[i].key == val) {
				(this->*(mm->entries[i].callback))();
				/* refresh the view */
				redraw();

				break;
			}
		}
		if (i == mm->entries.size())
			status_error("WRONG INPUT !");

		usleep(200000);
	}

	return 0;
} 

uiapp::uiapp ()
{
	cout << vt100_clear_scr;
	cout << vt100_hide_cursor;

	struct winsize w;
	ioctl(0, TIOCGWINSZ, &w);

	fm = new uiframe_main(w.ws_col, w.ws_row);
}

int uiapp::exec()
{
	return fm->exec();
}
